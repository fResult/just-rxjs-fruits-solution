# Exercise 6: Map
Some fruits are dirty in this delivery.

The map operator enables data to be changed here. Wash the fruits by removing the dirt from dirty fruits. ([Learn more about map](https://rxjs.dev/api/operators/map))

## 🧾 Recipe
All apples and bananas should be cleared of dirt. (Note: use map)
1. Apple
2. Apple
3. Banana
4. Banana

## Ref:
https://www.rxjs-fruits.com/map
