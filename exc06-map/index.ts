import { from, map } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { cleanFruit } from "../utils/list-operator"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  "dirty-apple",
  "apple",
  "dirty-banana",
  "banana",
])

fruits.pipe(
  debugBefore(),
  map(cleanFruit),
  debugAfter(),
).subscribe(toConveyorBelt)
