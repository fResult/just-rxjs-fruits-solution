# Exercise 4: Take
That is too much of a good thing!

Use the take operator to put only as much bananas on the conveyor belt as is specified in the recipe. ([Learn more about take](https://rxjs.dev/api/operators/take))

## 🧾 Recipe
Only two bananas should be mixed. (Note: use take)
1. Banana
2. Banana

## Ref:
https://www.rxjs-fruits.com/take
