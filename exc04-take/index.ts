import { from, take } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  "banana",
  "banana",
  "banana",
  "banana",
])

fruits.pipe(
  debugBefore(),
  take(2),
  debugAfter(),
).subscribe(toConveyorBelt)
