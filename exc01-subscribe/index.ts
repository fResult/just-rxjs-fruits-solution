import { EMPTY } from "rxjs"
import { toConveyorBelt } from "../utils/toConveyor"

const conveyorBelt = EMPTY
conveyorBelt.subscribe(toConveyorBelt)
