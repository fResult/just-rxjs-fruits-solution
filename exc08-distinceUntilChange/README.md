# Exercise 8: distinctUntilChanged
One after the other! Some fruits come twice in a row. However, we need the fruits alternately.

In our case, the distinctUntilChanged operator helps us. It prevents duplicate fruits from being delivered one after the other in our data stream. ([Learn more about distinctUntilChanged](https://rxjs.dev/api/operators/distinctUntilChanged))

## 🧾 Recipe
No fruit should be mixed twice in a row. (Note: use distinctUntilChanged)
1. Banana
2. Apple
3. Banana

## Ref:
https://www.rxjs-fruits.com/distinctuntilchanged
