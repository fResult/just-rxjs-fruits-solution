import { distinctUntilChanged, from, take } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  'banana',
  'banana',
  'banana',
  'apple',
  'banana',
  'apple',
  'apple',
  'banana',
])

fruits.pipe(
  debugBefore(),
  distinctUntilChanged(),
  take(3),
  debugAfter(),
).subscribe(toConveyorBelt)
