# Exercise 13: skipLast
We can do without the last two fruits.

The skipLast operator enables us to ignore the last fruits. ([Learn more about skipLast](https://rxjs.dev/api/operators/skipLast))

## 🧾 Recipe
Add only the fruits specified on the recipe. (Note: use skipLast)
1. Apple
2. Apple
3. Banana

## Ref:
https://www.rxjs-fruits.com/skip-last
