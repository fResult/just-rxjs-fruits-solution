import { from, skipLast, tap } from "rxjs"
import { toConveyorBelt } from "../utils/toConveyor"
import { debugAfter, debugBefore } from "../utils/debugging"

const fruits = from([
  "apple",
  "apple",
  "banana",
  "apple",
  "banana"
])

fruits.pipe(
  debugBefore(),
  skipLast(2),
  debugAfter(),
).subscribe(toConveyorBelt)
