# Exercise 12: takeLast
Only the last please!

Use the takeLast operator to put only a certain number of the last fruits on the conveyor belt. ([Learn more about takeLast](https://rxjs.dev/api/operators/takeLast))

## 🧾 Recipe
Only the last three fruits should be mixed. (Note: use takeLast)
1. Banana
2. Apple
3. Banana

## Ref:
https://www.rxjs-fruits.com/take-last
