import { from, takeLast } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  "apple",
  "apple",
  "banana",
  "apple",
  "banana",
])

fruits.pipe(
  debugBefore(),
  takeLast(3),
  debugAfter(),
).subscribe(toConveyorBelt)
