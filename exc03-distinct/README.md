# Exercise 3: Distinct
Oh no! We get more fruit than the recipe says.

The pipe operator enables us to execute different RxJS operators one after the other in order to get our fruits as we need them.

In our case, the distinct operator helps us. It prevents duplicate fruits from being delivered in our data stream. ([Learn more about distinct](https://rxjs.dev/api/operators/distinct))

## 🧾 Recipe
Tip: Press the F12 key and use the console within the developer tools of the browser for debugging.  
Each fruit should only be mixed once. (Note: use distinct)
1. Apple
2. Banana

## Ref:
https://www.rxjs-fruits.com/distinct
