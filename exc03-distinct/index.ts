import { distinct, from } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  "apple",
  "apple",
  "banana",
  "apple",
])

fruits.pipe(
  debugBefore(),
  distinct(),
  debugAfter(),
).subscribe(toConveyorBelt)
