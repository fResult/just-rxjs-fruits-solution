import { from, map, merge, skip, skipLast } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { cleanFruit } from "../utils/list-operator"
import { toConveyorBelt } from "../utils/toConveyor"

const apples = from([
  "apple",
  "dirty-apple",
  "apple",
  "old-apple",
  "old-apple",
])
const bananas = from([
  "old-banana",
  "old-banana",
  "dirty-banana",
  "dirty-banana",
  "dirty-banana",
])

const freshApples$ = apples.pipe(
  debugBefore(),
  skipLast(2),
  map(cleanFruit),
  debugAfter(),
);

const freshBananas$ = bananas.pipe(
  debugBefore(),
  skip(2),
  map(cleanFruit),
  debugAfter(),
);

merge(freshApples$, freshBananas$).subscribe(toConveyorBelt)
