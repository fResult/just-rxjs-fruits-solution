# Exercise 14: skipLast, skip & merge
Don't give chaos a chance!

You received two deliveries. Take only usable fruit with the skipLast and skip operator. Then replace the empty observable with the merge function. Towards the end, clean up the dirty fruits and place them on the conveyor belt.

## 🧾 Recipe
Tip: Replace the EMPTY Observable with the merge function.  
Tip: Press the F12 key and use the console within the developer tools of the browser for debugging.  
Add only the fruits that are specified on the recipe. (Note: use skipLast, skip, merge and map)
1. Apple
2. Apple
3. Apple
4. Banana
5. Banana
6. Banana

## Ref:
https://www.rxjs-fruits.com/skiplast-skip-merge
