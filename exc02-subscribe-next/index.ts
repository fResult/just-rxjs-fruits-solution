import { from } from "rxjs"
import { toConveyorBelt } from "../utils/toConveyor"
import { debugAfter, debugBefore } from "../utils/debugging"

const fruits = from([
  "apple",
  "banana",
  "cherry",
])

fruits.pipe(debugBefore(), debugAfter()).subscribe(toConveyorBelt)
