# Exercise 2: subscribe-next
Now it's time to put some fruits on the conveyor belt!

The from function creates an observable from an array. This delivers the data one after the other, like a foreach loop.

The subscribe function expects a callback function as a parameter. This receives the data from the observable via the parameters.

## 🧾 Recipe
Subscribe to the Observable and put each fruit on the conveyor belt with the toConveyorBelt function.  
Add only the fruits that are specified on the recipe. (Note: use subscribe with next)
1. Apple
2. Banana
3. Cherry

## Ref:
https://www.rxjs-fruits.com/subscribe-next
