import { filter, from, map, take } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { cleanFruit, isFreshFruit } from "../utils/list-operator"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  "old-banana",
  "apple",
  "dirty-banana",
  "apple",
])

fruits.pipe(
  debugBefore(),
  take(3),
  filter(isFreshFruit),
  map(cleanFruit),
  debugAfter(),
).subscribe(toConveyorBelt)
