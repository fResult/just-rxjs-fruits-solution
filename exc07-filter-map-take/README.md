# Exercise 7: filter-map-take
What a mess!

You just want an apple and a banana. However, the delivery also includes old and dirty fruit. Now use all previously learned operators filter, map and take one after the other in the pipe function.

## 🧾 Recipe
Tip: Press the F12 key and use the console within the developer tools of the browser for debugging.
Add only the fruits that are specified on the recipe. (Note: use filter, map and take)
1. Apple
2. Banana

## Ref:
https://www.rxjs-fruits.com/filter-map-take
