import { filter, from, merge } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { isFreshFruit } from "../utils/list-operator"
import { toConveyorBelt } from "../utils/toConveyor"

const apples = from([
  "apple",
  "apple",
  "old-apple",
  "apple",
  "old-apple",
])

const bananas = from([
  "banana",
  "old-banana",
  "old-banana",
  "banana",
  "banana",
])

const freshApple$ = apples.pipe(
  debugBefore(),
  filter(isFreshFruit),
  debugAfter(),
)

const freshBanana$ = bananas.pipe(
  debugBefore(),
  filter(isFreshFruit),
  debugAfter(),
)

merge(freshApple$, freshBanana$).subscribe(toConveyorBelt)
