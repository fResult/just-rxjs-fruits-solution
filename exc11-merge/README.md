# Exercise 11: merge
Now we have to combine two deliveries.

Our fruit supplier had to deliver the fruit to us separately. The merge function can combine different observables into one observable. Then we can use the pipe function to put only fresh fruit on the conveyor belt. ([Learn more about merge](https://rxjs.dev/api/index/function/merge))

## 🧾 Recipe
Tip: Replace the EMPTY Observable with the merge function.  
Add only the fruits that are specified on the recipe. (Note: use merge and filter)
1. Apple
2. Apple
3. Apple
4. Banana
5. Banana
6. Banana

## Ref:
https://www.rxjs-fruits.com/merge
