import { from, repeat, tap } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from<string[]>(["apple"])

fruits.pipe(
  debugBefore(),
  repeat(3),
  debugAfter(),
).subscribe(toConveyorBelt)
