# Exercise 16: repeat
Much too little!

The delivery really only brought us an apple. The recipe gives us three apples need. In this emergency, the repeat operator helps us. He repeats the observable with the specified number. ([Learn more about repeat](https://rxjs.dev/api/operators/repeat))

## 🧾 Recipe
Add only the fruits specified on the recipe. (Note: use `repeat`)
1. Apple
2. Apple
3. Apple
