# Exercise 10: skip-take-map
An excessive delivery!

You just want a banana. However, the delivery contains too much unnecessary fruit. Now use all operators that have been learned so far skip, take and map one after the other in the pipe function.

## 🧾 Recipe
Tip: Press the F12 key and use the console within the developer tools of the browser for debugging.
Add only the fruits that are specified on the recipe. (Note: use skip, take and map)
1. Banana

## Ref:
https://www.rxjs-fruits.com/skip-take-map
