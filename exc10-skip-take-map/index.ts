import { from, map, skip, take } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { cleanFruit } from "../utils/list-operator"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  "dirty-apple",
  "apple",
  "dirty-banana",
  "dirty-banana",
  "apple",
])

fruits.pipe(
  debugBefore(),
  skip(2),
  take(1),
  map(cleanFruit),
  debugAfter(),
).subscribe(toConveyorBelt)
