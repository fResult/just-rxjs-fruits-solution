# Exercise 9: skip
We can do without the first two fruits. The skip operator enables us to skip unnecessary fruit. ([Learn more about skip](https://rxjs.dev/api/operators/skip))

## 🧾 Recipe
Add only the fruits specified on the recipe. (Note: use skip)
1. Banana
2. Apple

## Ref:
https://www.rxjs-fruits.com/skip
