export function isFreshFruit(fruit: string): boolean {
  return !fruit.match(new RegExp('old'))
}

export function cleanFruit(fruit: string): string {
  return fruit.replace('dirty-', '')
}

export function identity<T>(x: T): T {
  return x
}
