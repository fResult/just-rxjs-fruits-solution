import { tap } from "rxjs"
import type { MonoTypeOperatorFunction } from "rxjs"

export function debugBefore(): MonoTypeOperatorFunction<string> {
    return tap<string>((fruit) => console.log("before:", fruit))
}

export function debugAfter(): MonoTypeOperatorFunction<string> {
  return tap<string>((fruit) => console.log("after:", fruit))
}
