# Exercise 15: zip & concatMap
Variety is the key to happiness!

We have received two deliveries and have to alternately put the contents on the conveyor belt. The zip function connects two observables and passes the data on to the pipe function alternately. However, not every fruit individually, but as a multi-dimensional array. ([Learn more about zip](https://rxjs.dev/api/index/function/zip))

The concatMap operator projects the multidimensional array into an observable and returns the fruit individually. ([Learn more about concatMap](https://rxjs.dev/api/operators/concatMap))

## 🧾 Recipe
Tip: Replace the EMPTY Observable with the zip function.  
Tip: Press the F12 key and use the console within the developer tools of the browser for debugging.  
Each fruit should be mixed alternately. (Note: Use zip and concatMap)
1. Apple
2. Banana
3. Apple
4. Banana

## Ref:
https://www.rxjs-fruits.com/zip-concatmap
