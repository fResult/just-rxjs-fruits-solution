import { concatMap, from, identity, zip } from "rxjs"
import { debugAfter } from "../utils/debugging"
import { toConveyorBelt } from "../utils/toConveyor"

const apples = from(["apple", "apple"])
const bananas = from(["banana", "banana"])

zip(apples, bananas).pipe(
  concatMap(identity),
  debugAfter(),
).subscribe(toConveyorBelt)

// from([[1,3],[3,5],[5,7]]).pipe(
//     tap((x) => console.log('before', x)),
//     concatMap(([n,m]) => [n,m]),
//     tap((x) => console.log('after', x))
// ).subscribe()
