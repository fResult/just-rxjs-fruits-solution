import { filter, from } from "rxjs"
import { debugAfter, debugBefore } from "../utils/debugging"
import { isFreshFruit } from "../utils/list-operator"
import { toConveyorBelt } from "../utils/toConveyor"

const fruits = from([
  "apple",
  "apple",
  "old-apple",
  "apple",
  "old-apple",
  "banana",
  "old-banana",
  "old-banana",
  "banana",
  "banana",
])

fruits.pipe(
  debugBefore(),
  filter(isFreshFruit),
  debugAfter(),
).subscribe(toConveyorBelt)
