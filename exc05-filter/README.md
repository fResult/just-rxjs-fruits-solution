# Exercise 5: filter
Yuck! Old fruit has been delivered here.

Use the filter operator to put only fresh fruit on the conveyor belt. ([Learn more about the filter](https://rxjs.dev/api/operators/filter))

## 🧾 Recipe
All fresh apples and bananas should be mixed. (Note: use filter)
1. Apple
2. Apple
3. Apple
4. Banana
5. Banana
6. Banana

## Ref:
https://www.rxjs-fruits.com/filter
